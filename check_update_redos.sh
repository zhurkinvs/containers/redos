#!/usr/bin/env bash

dnf check-update > /dev/null 2>&1
return_code=$?

if [ $return_code -eq 100 ]; then
  dnf check-update > update_packages.txt
  echo $return_code > updates.txt
elif [ $return_code -eq 0 ]; then
  > update_packages.txt
  echo $return_code > updates.txt
else
  > update_packages.txt
  echo $return_code > updates.txt
fi
